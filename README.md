# IPerfEcT Tutorial

Tutorial Material presented during HiPEAC 2021.

GitLab repository [https://gitlab.com/lappsufrn/iperfect](https://gitlab.com/lappsufrn/iperfect)

Youtube video [IPerfEcT Tutorial](https://youtu.be/KP8kthI_iNc)
