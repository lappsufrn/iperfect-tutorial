#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

void mm(int* MatrixA, int* MatrixB, int* MatrixC, int N,int t) {
        #pragma openmp parallel for schedule(dynamic) num_threads(t)
	for (int i = 0; i < N; ++i) {
 		for (int j = 0; j < N; ++j) {
			int sum = 0;
			for (int k = 0; k < N; ++k)
				sum += MatrixA[i * N + k] * MatrixB[k * N + j];
			MatrixC[i * N + j] = sum;
		}
	}
}

int main(int argc, char** argv) {

	srand(time(NULL));
	
	if (argc != 3) {
		std::cerr << "./executable N Threads" << std::endl;
        exit(-1);
    }
    
    const int N = atoi(argv[1]);
    const int t = atoi(argv[2]);
	
    int* MatrixA = new int[N*N];
    int* MatrixB = new int[N*N];
    int* MatrixC = new int[N*N];
	
	for (int i = 0; i < N * N; ++i) {
		MatrixA[i] = ((float) rand() / RAND_MAX) * 100;
		MatrixB[i] = ((float) rand() / RAND_MAX) * 100;
	}

    mm(MatrixA, MatrixA, MatrixC, N,t);

    delete[] MatrixA;
    delete[] MatrixB;
    delete[] MatrixC;
	std::cout << "Ok\n\n";
}
